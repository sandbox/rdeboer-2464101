
Openlayers Center Override
==========================

A "behavior" plugin for Openlayers maps that allows you to set the center and
zoom level of a map via parameters appended to the URL of the page that the map
appears on.

There are three ways to specify the center:

a) By appending "?center=LON,LAT" to the URL.
b1) By appending "?ref-node=ID" to the URL, whereby ID is the node number of a
   node that has a Geofield on it; the module will use that node's lon,lat as
   the center of the map you are displaying.
   "?ref-node=ID" may be generated manually or automatically, for instance via
   the Node Reference URL Widget, www.drupal.org/project/nodereference_url.
   Using the Node Reference URL Widget, you can create links to create nodes
   with maps that use as their center the parent node they have a node reference
   to.
b2) By appending the ID as a URL argument: /ID

The zoom level is also taken from the URL, "zoom=Z", where Z is an integer in
the range 0..21, subject to what your map supports.
If omitted, the zoom level on the Center Override behavior config tab is used
instead.

Examples
--------
If you want to send someone an email with a link to a page containing a map on
your site and you want that map correctly positioned and zoomed when the
recipient opens the link, you'd use something like this:

  example.com/my-page-with-map?center=144.9,-37.7&zoom=10

If you use a node reference field to create child-to-parent relationship and
have installed the Node Reference URL Widget module, it will auto-generate on
every parent node, a link to create a child:

  node/add/article?ref-node=123

or equivalently:

  node/add/article/123

If the child node contains an Openlayers map or map widget, then you can center
that map based on the Geofield that resides on the parent, using the above link.
You may append a zoom level too:

  node/add/article/123?zoom=16

If not specified, then the zoom level is taken from the behaviour configuration
UI belonging to this module.


Installation and configuration
------------------------------
1) Install and enable this module like any other module.
2) Visit admin/structure/openlayers/maps and press Edit next to map that needs
   to have its center overridden
3) Under the "Behaviors" tab find the fieldset “Center override”.
4) Flick it on and enter the "URL parameter name" and "Initial zoom level"
