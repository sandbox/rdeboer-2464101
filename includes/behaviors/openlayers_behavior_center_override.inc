<?php
/**
 * @file
 * Implementation of openlayers_behavior.
 */

/**
 * Openlayers Center Override Behavior
 */
class openlayers_behavior_center_override extends openlayers_behavior {

  function options_init() {
    return array(
      'url_par' => 'node-ref',
      'zoom' => '',
    );
  }

  function options_form($defaults = array()) {
    $intials = $this->options_init();
    $description1 = t('If you use this module to center maps via the <em>center=LON,LAT</em> URL parameter, then you can leave this field blank.');
    $description2 = t('Otherwise enter the URL parameter name, as in <em>name=###</em>, of the pages that contain maps that you wish to be centered based on other content.');
    $example = t('So, if you are creating maps using a URL like <em>node/add/content-type?ref-node=123</em>, then enter <em>ref-node</em>');
    $disclaimer = t('If you are not sure what to put here, blank out the field and it still may work, if the content ID appears as part of the URL as an argument, i.e. following a slash.');

    $options = array(
      'url_par' => array(
        '#title' => t('URL parameter name'),
        '#type' => 'textfield',
        '#description' => $description1 . '<br/>' . $description2 . ' ' . $example . '<br/>' . $disclaimer,
        '#default_value' => isset($defaults['url_par']) ? check_plain($defaults['url_par']) : $intials['url_par'],
      ),
      'zoom' => array(
        '#title' => t('Initial zoom level'),
        '#type' => 'textfield',
        '#size' => 2,
        '#description' => t('An integer number, this zoom level must be supported by the map you are using. Typical ranges are 0..18 and 0..21.') .
          '<br/>' . t('Leave blank if you do not wish to override the zoom level that may be specified elsewhere.'),
        '#default_value' => isset($defaults['zoom_level']) ? check_plain($defaults['zoom_level']) : $intials['zoom_level'],
      ),
    );
    return $options;
  }

  /**
   * Render, kind of.
   *
   * Modifies the map object, setting the zoom level specified in the UI with
   * the center taken from the node ID refereced in the URL.
   */
  function render(&$map) {

    if ($zoom = filter_input(INPUT_GET, 'zoom')) {
      $map['center']['initial']['zoom'] = (int)$zoom;
    }
    elseif (!empty($this->options['zoom'])) {
      $map['center']['initial']['zoom'] = (int)$this->options['zoom'];
    }

    if ($center = filter_input(INPUT_GET, 'center')) {
      $map['center']['initial']['centerpoint'] = $center;
    }
    if (!empty($center)) {
      return $this->options;
    }

    // Try to find the node ID on the URL, at the specified parameter name.
    $id = filter_input(INPUT_GET, trim($this->options['url_par']));

    // If not found as a URL parameter, try finding the node ID as an argument.
    if (empty($id)) {
      foreach (arg() as $arg) {
        if (is_numeric($arg)) {
          $id = $arg;
          break;
        }
      }
    }
 
    if ($id && ($node = node_load($id))) {
      // Should perhaps use Field API, but doing it quick and dirty...
      // Simply find the first field on the node that has a 'lon' and 'lat' and
      // use that as the center.
      foreach ($node as $field) {
        if (is_array($field)) {
          $field_values = reset($field);
          if (is_array($field_values)) {
            $first_value = reset($field_values);
            if ($first_value && isset($first_value['lon']) && isset($first_value['lat'])) {
              $node_lonlat = $first_value['lon'] . ',' . $first_value['lat'];
              break;
            }
          }
        }
      }
      if (isset($node_lonlat)) {
        $map['center']['initial']['centerpoint'] = $node_lonlat;
      }
      else {
        drupal_set_message(t("Content with ID '%id' was found, but does not appear to contain a field with longitude and latitude values. Cannot re-center the map.", array('%id' => $id)), 'warning');
      }
    }
    else {
      //drupal_set_message(t("No content with ID '%id' was found. Cannot re-center the map.", array('%id' => $id)), 'error');
    }
    return $this->options;
  }

}
